import check_in
from check_out import checkout
from reading import reading_
from change_status import changestatus


def display_rooms():
    ans = True
    while ans==True:
        print(
        """
        1) Display rooms information
        2) Customer check in
        3) Customer check out
        4) Exit/Quit
        """)
        ans= input("Choose available options: ")
        if ans=="1":
            for i in reading_():
                print(
                "|","Room:",i[0],"|","Number of People:",i[1],"|","Price:",i[2]," |","Status:",i[3])
            display_rooms()
        elif ans=="2":
            check_in.checkin()
        elif ans=="3":
            checkout()
        elif ans=="4":
            print("\n Goodbye!")
            break
        else:
            print("\n It is not a valid choice try again.\n")
            display_rooms()
