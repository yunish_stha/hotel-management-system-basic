import datetime
import random
from reading import reading_
from change_status import changestatus
#from display import display_rooms
import display
def checkin():
    #Collecting client information
    ans = False
    while ans == False:
        input_room = input("Enter client's desired room: ")
        for room in reading_(): #iterate rooms through reading
            if(input_room == room[0] and room[3] == "free"): #check if entered room is correct and it is free
                print("Good choice!!")
                ans = True
                break
        else:
            print("Sorry! Room not found/selected room is occupied")
    try:
        rooms = []
        for room in reading_():
            rooms.append(room[0]) #Adding file to rooms[]
        if input_room in rooms: #Check if entered room is in rooms
            print ("Current date and time: " , datetime.datetime.now())
            name = input("Enter full name: ")
            address = input("Enter address: ")
            phone = input("Enter phone: ")
            changestatus(input_room)
            current_datetime= str(datetime.datetime.now())
            fname = name + str(random.randint(100000, 1000000)) + ".txt"
            file = open(fname, 'w')
            file.write("Customer name: "+name+"\n")
            file.write("Address: "+address+"\n")
            file.write("Phone: "+phone+"\n")
            file.write("Current date and time: "+current_datetime+"\n")
            file.close()
            print("Checkin Successful")
            ask = input("Do you want to continue y/n: ")
            if (ask=="y"):
                 checkin()
            else:
                display.display_rooms()
    except:
        display.display_rooms()
