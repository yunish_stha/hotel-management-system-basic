import random
import time
import datetime
from reading import reading_
from display import display_rooms
from check_in import checkin
from check_out import checkout
from change_status import changestatus

print(
"""
##### +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #####
##### +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #####
##### ++++++++++++++++ WELCOME TO HOTEL MANAGEMENT SYSTEM +++++++++++++ #####
##### +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #####
##### +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #####
"""
)
rooms = reading_()
display = display_rooms()
